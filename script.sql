-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ticketDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ticketDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ticketDB` DEFAULT CHARACTER SET utf8 ;
USE `ticketDB` ;

-- -----------------------------------------------------
-- Table `ticketDB`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`customer` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`customer` (
  `customer_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
   `last_name` VARCHAR(45) NOT NULL,
  `age` INT NOT NULL,
  PRIMARY KEY (`customer_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`country` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`country` (
  `country_id` INT NOT NULL AUTO_INCREMENT,
  `country_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`country_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`delivery_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`delivery_type` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`delivery_type` (
  `delivery_type_id` INT NOT NULL AUTO_INCREMENT,
  `delivery_type_name` VARCHAR(45) NOT NULL,
  `day` INT NOT NULL,
  PRIMARY KEY (`delivery_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`event_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`event_type` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`event_type` (
  `event_type_id` INT NOT NULL AUTO_INCREMENT,
  `event_type_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`event_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`artist` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`artist` (
  `artist_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `country_id` INT NOT NULL,
  PRIMARY KEY (`artist_id`),
  INDEX `fk_artist_country1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_artist_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `ticketDB`.`country` (`country_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`event` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`event` (
  `event_id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `free_places` INT NOT NULL,
  `event_type_id` INT NOT NULL,
  `artist_id` INT NOT NULL,
  PRIMARY KEY (`event_id`),
  INDEX `fk_event_event_type1_idx` (`event_type_id` ASC) VISIBLE,
  INDEX `fk_event_artist1_idx` (`artist_id` ASC) VISIBLE,
  CONSTRAINT `fk_event_event_type1`
    FOREIGN KEY (`event_type_id`)
    REFERENCES `ticketDB`.`event_type` (`event_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_artist1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `ticketDB`.`artist` (`artist_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`ticket_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`ticket_type` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`ticket_type` (
  `ticket_type_id` INT NOT NULL AUTO_INCREMENT,
  `ticket_type_name` VARCHAR(45) NULL,
  PRIMARY KEY (`ticket_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`price`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`price` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`price` (
  `price_id` INT NOT NULL AUTO_INCREMENT,
  `cost` DECIMAL NOT NULL,
  `event_id` INT NOT NULL,
  `ticket_type_id` INT NOT NULL,
  PRIMARY KEY (`price_id`, `event_id`, `ticket_type_id`),
  INDEX `fk_price_event1_idx` (`event_id` ASC) VISIBLE,
  INDEX `fk_price_ticket_type1_idx` (`ticket_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_price_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `ticketDB`.`event` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_price_ticket_type1`
    FOREIGN KEY (`ticket_type_id`)
    REFERENCES `ticketDB`.`ticket_type` (`ticket_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`status_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`status_type` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`status_type` (
  `status_type_id` INT NOT NULL AUTO_INCREMENT,
  `status_type_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`status_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`oplata_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`oplata_type` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`oplata_type` (
  `oplata_type_id` INT NOT NULL AUTO_INCREMENT,
  `oplata_type_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`oplata_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ticketDB`.`ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ticketDB`.`ticket` ;
CREATE TABLE IF NOT EXISTS `ticketDB`.`ticket` (
  `ticket_id` INT NOT NULL AUTO_INCREMENT,
  `place_number` VARCHAR(45) NOT NULL,
  `status_type_id` INT NOT NULL,
  `oplata_type_id` INT NOT NULL,
  `delivery_type_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  `price_id` INT NOT NULL,
  `event_id` INT NOT NULL,
  `ticket_type_id` INT NOT NULL,
  PRIMARY KEY (`ticket_id`),
  INDEX `fk_ticket_status_type1_idx` (`status_type_id` ASC) VISIBLE,
  INDEX `fk_ticket_oplata_type1_idx` (`oplata_type_id` ASC) VISIBLE,
  INDEX `fk_ticket_delivery_type1_idx` (`delivery_type_id` ASC) VISIBLE,
  INDEX `fk_ticket_customer1_idx` (`customer_id` ASC) VISIBLE,
  INDEX `fk_ticket_price1_idx` (`price_id` ASC, `event_id` ASC, `ticket_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_ticket_status_type1`
    FOREIGN KEY (`status_type_id`)
    REFERENCES `ticketDB`.`status_type` (`status_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_oplata_type1`
    FOREIGN KEY (`oplata_type_id`)
    REFERENCES `ticketDB`.`oplata_type` (`oplata_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_delivery_type1`
    FOREIGN KEY (`delivery_type_id`)
    REFERENCES `ticketDB`.`delivery_type` (`delivery_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `ticketDB`.`customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_price1`
    FOREIGN KEY (`price_id` , `event_id` , `ticket_type_id`)
    REFERENCES `ticketDB`.`price` (`price_id` , `event_id` , `ticket_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
insert into ticket_type(ticket_type_id,ticket_type_name) values
(1,'VIP'),
(2,'ordinary'),
(3,'one-day pass'); 
insert into status_type(status_type_id,status_type_name)values
(1,'reserved'),
(2,'pending'),
(3,'open');
insert into oplata_type(oplata_type_id,oplata_type_name)values
(1,'credit card'),
(2,'cash'),
(3,'paypal');
insert into event_type(event_type_id,event_type_name)values
(1,'rock consert'),
(2,'chamber orchestra'),
(3,'opera'),
(4,'festival');
insert into delivery_type(delivery_type_id,delivery_type_name,day)values
(1,'courier',2),
(2,'ukrposhta',3),
(3,'electrnic',1);
insert into country(country_id,country_name)values
(1,'Ukraine'),
(2,'Poland'),
(3,'USA'),
(4,'Italian');
insert into customer(customer_id,first_name,last_name,age)values
(1,'Yura','Savchenko',18),
(2,'Bogdan','Pelikan',23),
(3,'Viktor','Leskov',19);
insert into artist(artist_id,first_name,last_name,country_id)values
(1,'Daniel','Reynolds',3),
(2,'Vitaliy','Sarazhinsky',1),
(3,'Selena','Gomes',3);
insert into event(event_id,date,free_places,event_type_id,artist_id)values
(1,'2019-12-11',20,2,2),
(2,'2020-02-02',50,4,3),
(3,'2020-04-11',100,1,1),
(4,'2019-10-29',200,4,1);
insert into price(price_id,cost,event_id,ticket_type_id)values
(1,800,1,1),
(2,700,1,2),
(3,600,1,3),
(4,1000,2,1),
(5,850,2,2),
(6,600,2,3),
(7,500,3,3);
insert into ticket(ticket_id,place_number,status_type_id,oplata_type_id,delivery_type_id,customer_id,price_id,event_id,ticket_type_id)values
(1,'150',1,1,1,1,1,1,1),
(2,'80',2,2,2,2,7,3,3),
(3,'200',3,3,3,3,4,2,1);

